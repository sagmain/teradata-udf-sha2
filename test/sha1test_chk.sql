/* check sha1 result against test data */
sel
i,
hash_sha1(input_data),
case
 when hash_sha1(input_data) = output_data then 'OK'
 else 'NG'
end as "result"
from
sha1_test
order by 1;
