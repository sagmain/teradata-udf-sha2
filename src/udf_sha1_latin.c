/* Copyright (c) 2008 by Teradata Corporation. All Rights Reserved. */

#define SQL_TEXT Latin_Text
#include "sqltypes_td.h"
#include <stdlib.h>
#include <string.h>

#define DIGEST_LEN 20
#define UDF_OK "00000"

void sha1(const unsigned char message[], int len, unsigned char result[]);

void sha1_latin(VARCHAR_LATIN *arg, CHARACTER_LATIN *result,
 char sqlstate[])
{
    int i;
    unsigned char outbuf[DIGEST_LEN];

    sha1((unsigned char *)arg, strlen((char *)arg), outbuf);

    for (i = 0; i < DIGEST_LEN; i++) {
        sprintf(result + i*2, "%02X", outbuf[i]);
    }

    sprintf(sqlstate, UDF_OK);
}
