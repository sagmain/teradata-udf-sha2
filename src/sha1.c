/* Copyright (c) 2008 by Teradata Corporation. All Rights Reserved. */

/* http://www.itl.nist.gov/fipspubs/fip180-1.htm */

#include <stdlib.h>
#include <string.h>

static void sha1_block(unsigned int h[], const unsigned int blk[]);
static unsigned int f(int t, unsigned int b, unsigned int c, unsigned int d);

#define BLOCK_SIZE 64

/* f(t; B, C, D) */
#define F0(B, C, D) (((B) & (C)) | ((~B) & (D)))
#define F1(B, C, D) ((B) ^ (C) ^ (D))
#define F2(B, C, D) (((B) & (C)) | ((B) & (D)) | ((C) & (D)))

/* K(t) */
static unsigned int K[] = {
    0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6
};

/* H */
static unsigned int H[] = {
    0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0
};

#define LROT(x, s) (((x) << (s)) | ((x) >> (32 - (s))))

#define FINAL_BLOCK_SIZE (BLOCK_SIZE - 8)
#define SHA1_ROUNDS 80

static unsigned int f(int phase, unsigned int b, unsigned int c,
                      unsigned int d)
{
    if (phase == 0)
        return F0(b, c, d);
    else if (phase == 2)
        return F2(b, c, d);
    else /* (phase == 1 || phase == 3) */
        return F1(b, c, d);
}

#define getbyte(v, i) (((v) & (0xFF << (i) * 8)) >> (i) * 8)
#define B(v, i) ((v) & (0xFF << (i) * 8))

unsigned int reverse_endian(unsigned int w)
{
    return (B(w, 0) << 24) | (B(w, 1) << 8) | (B(w, 2) >> 8) | (B(w, 3) >> 24);
}


void sha1(const unsigned char message[], int len, unsigned char result[])
{
    int pos = 0;
    int remain = 0;
    int padded = 0;
    int i, j;

    unsigned int X[BLOCK_SIZE / sizeof(int)]; /* 512bit block = 32bit * 16 */
    const unsigned char pad = 1 << 7; // first byte of padding
    unsigned int la[2];
    unsigned char buf[BLOCK_SIZE]; // for last/carry block
    unsigned int h[] = { H[0], H[1], H[2], H[3], H[4] }; // sha1 buffer

    memset(buf, 0, sizeof(buf));

    while (len - pos >= BLOCK_SIZE) {
        memcpy(X, &message[pos], sizeof(X));
        sha1_block(h, X);
        pos += BLOCK_SIZE;
    }

    remain = len - pos;
    if (remain > 0) {
        memcpy(buf, &message[pos], remain);
    }

    // cannot put length field in final block
    if (remain > FINAL_BLOCK_SIZE - 1) {
        buf[remain] = pad;
        memcpy(X, buf, sizeof(buf));
        sha1_block(h, X);
        padded = 1;
        memset(buf, 0, sizeof(buf));
    }

    // step1: padding
    if (!padded)
        buf[remain] = pad;

    // step2: append length
    la[1] = reverse_endian(len << 3); // byte to int
    la[0] = 0; // assuming length < 4Gb
    memcpy(buf + FINAL_BLOCK_SIZE, la, sizeof(la));
    
    // run final block
    memcpy(X, buf, sizeof(buf));
    sha1_block(h, X);

    for (i = 0; i < 5; i++) {
        for (j = 0; j < 4; j++) {
            result[i*4 + j] = getbyte(h[i], 3 - j);
        }
    }

    /* clear h to maintain security */
    memset(h, 0, sizeof(h));
}


static void sha1_block(unsigned int h[], const unsigned int blk[])
{
    unsigned int a = h[0];
    unsigned int b = h[1];
    unsigned int c = h[2];
    unsigned int d = h[3];
    unsigned int e = h[4];
    unsigned int W[SHA1_ROUNDS];

    int i;
    int phase = 0;
    
    for (i = 0; i < 16; i++) {
        W[i] = reverse_endian(blk[i]);
    }

#ifdef DEBUG
    printf("sha1_block <\n");
    for (i = 0; i < 16; i++) {
        printf(" %08x", W[i]);
        if (i > 0 && i % 4 == 0)
            printf("\n");
    }
    printf("\n");
    printf("a, b, c, d, e = %x %x %x %x %x\n", a, b, c, d, e);
#endif

    for (i = 16; i < SHA1_ROUNDS; i++) {
        W[i] = LROT(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
    }

    for (i = 0; i < SHA1_ROUNDS; i++) {
        unsigned int t = LROT(a, 5) + f(phase, b, c, d) + e + W[i] + K[phase];
        e = d;
        d = c;
        c = LROT(b, 30);
        b = a;
        a = t;
#ifdef DEBUG
        printf("%d(%d): a, b, c, d, e = %x %x %x %x %x\n", i, phase, a, b, c, d, e);
#endif
        if (i == 19 || i == 39 || i == 59)
            phase++;
    }

    h[0] += a;
    h[1] += b;
    h[2] += c;
    h[3] += d;
    h[4] += e;

#ifdef DEBUG
    printf("sha1_block > : %x %x %x %x %x\n", h[0], h[1], [2], h[3], h[4]);
#endif
}
