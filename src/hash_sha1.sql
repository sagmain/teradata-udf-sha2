/* script to install the UDF */

replace function hash_sha1
  (arg varchar(32000) character set latin)
  returns char(40) character set latin
  language c
  no sql
  external name 'cs:sha1:sha1.c:cs:sha1_latin:udf_sha1_latin.c:F:sha1_latin'
  parameter style td_general;
