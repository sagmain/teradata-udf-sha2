# teradata-udf-sha2

#### 介绍
基于Teradata数据仓库的sha1 sha256 sha512的加密算法

#### 软件架构
代码可存放到ETL主机，直接使用bteq命令登录Teradata数据仓库，执行.run file命令即可安装成功


#### 安装教程

1. sha1 安装，请参考 [README-sha1.md]: https://gitee.com/Hu-Lyndon/teradata-udf-sha2/blob/master/README-sha1.md 
2. sha256 sha512 安装，请参考 [README-sha256-512.md] https://gitee.com/Hu-Lyndon/teradata-udf-sha2/blob/master/README-sha256-512.md

#### 使用说明

请参考项目test路径下的sql文件
